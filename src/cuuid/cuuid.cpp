#include "casualuuid/cuuid.hpp"

#include <cstddef>
#include <cstdint>
#include <cstring>
#include <iomanip>
#include <ios>
#include <iostream>
#include <limits>
#include <memory>
#include <mutex>
#include <random>

namespace {

std::mutex rng_mutex;

auto generate_seed() -> uint64_t {
  std::random_device rnd_dev;
  auto time_now = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  return rnd_dev() ^ static_cast<uint64_t>( time_now );
}

template<typename T, int W = std::numeric_limits<T>::digits>
std::independent_bits_engine<std::mt19937_64, W, T> dist{ generate_seed() };

auto random_number() -> std::uint64_t {
  std::lock_guard<std::mutex> lck( rng_mutex );
  return dist<unsigned long long>(); // NOLINT(*-runtime-int) with uint64 is UB
}
} // namespace

namespace cuuid {

auto Uuid::byte_data() const -> data_type const & { return bytes; }

auto Uuid::create_uuid4() -> Uuid {
  Uuid uuid4{};

  std::uint64_t as_int = random_number();
  std::memcpy( uuid4.bytes.data(), &as_int, sizeof( as_int ) );

  as_int = random_number();
  std::memcpy( uuid4.bytes.data() + bytes_size / 2, &as_int, sizeof( as_int ) );

  // NOLINTBEGIN(*signed-bitwise) Using char s from unit8_t, which are always unsigned
  uuid4.bytes[ _version_index ] =
      ( uuid4.bytes[ _version_index ] & _version_mask ) | _version4;
  uuid4.bytes[ _reserved_index ] =
      ( uuid4.bytes[ _reserved_index ] & _reserved_mask ) | _reserved;
  // NOLINTEND(*signed-bitwise)

  return uuid4;
};

} // namespace cuuid

auto operator<<( std::ostream & dest, cuuid::Uuid uid ) -> std::ostream & {
  auto sflags = dest.flags();
  auto sfill = dest.fill();
  auto swidth = dest.width();

  dest << std::hex << std::setfill( '0' );
  auto interval = 4;
  auto sep_count = 0;
  for ( std::byte const dat : uid.byte_data() ) {
    dest << std::setw( 2 ) << std::to_integer<unsigned int>( dat );

    if ( sep_count < 4 && --interval == 0 ) {
      dest << '-';
      interval = 2;
      sep_count += 1;
    }
  }

  dest.flags( sflags );
  dest.fill( sfill );
  dest.width( swidth );
  return dest;
}
