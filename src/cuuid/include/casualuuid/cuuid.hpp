#pragma once

#include <array>
#include <cstdint>
#include <sstream>
#include <string>

namespace cuuid {

class Uuid {
  public:
    static constexpr std::size_t bytes_size = 16;
    using data_type = std::array<std::byte, bytes_size>;

    [[nodiscard]] auto byte_data() const -> data_type const &;

    static auto create_uuid4() -> Uuid;

  private:
    static std::size_t constexpr _version_index = 6;
    static std::byte constexpr _version_mask{ 0x0f };
    static std::byte constexpr _version4{ 0x40 };

    static std::size_t constexpr _reserved_index = 8;
    static std::byte constexpr _reserved_mask{ 0x3f };
    static std::byte constexpr _reserved{ 0x80 };

    data_type bytes;
};

} // namespace cuuid

auto operator<<( std::ostream & dest, cuuid::Uuid uid ) -> std::ostream &;
