#pragma once

#include <array>
#include <cstdint>
#include <optional>
#include <regex>
#include <string>
#include <unordered_map>
#include <vector>

namespace cuuid::util {

class Argument {
  public:
    enum class ArgType { Positional, Flag };

    // Positional argument
    Argument( std::string name, std::string description );

    // Flag
    Argument(
        std::string name, std::string description, std::string short_opt,
        std::string long_opt = {} );

    void match( std::string const & arg );
    [[nodiscard]] auto matched() const -> bool;

    [[nodiscard]] auto name() const -> std::string;
    [[nodiscard]] auto description() const -> std::string;
    [[nodiscard]] auto long_opt() const -> std::string;
    [[nodiscard]] auto short_opt() const -> std::string;

    [[nodiscard]] auto kind() const -> ArgType;

  private:
    std::string _name{};
    std::string _description{};
    std::string _short{};
    std::string _long{};

    ArgType _kind;

    bool _arg_matched = false;
};

class Parser {
  public:
    Parser( std::string banner, std::string version, std::string binary_name = {} );
    void set_banner( std::string banner );
    void set_version( std::string version );

    void add_positional_argument( std::string name, std::string description );

    void add_flag(
        std::string name, std::string description, std::string short_flag,
        std::string long_flag = {} );

    auto has_argument( std::string const & name ) -> bool;

    void parse( int argc, char ** argv );

    auto unmatched_args() const -> std::vector<std::string> const &;

    void print_help();
    void print_version();

  private:
    std::string _binary_name;
    std::string _banner;
    std::string _version;

    std::unordered_map<std::string, Argument> _args = {};
    std::vector<std::string> _unknown_args = {};
};

} // namespace cuuid::util
