#include "casualuuid/argparser.hpp"

#include <cstddef>
#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

namespace cuuid::util {

Argument::Argument( std::string name, std::string description )
    : _name( std::move( name ) ), _description( std::move( description ) ),
      _kind( ArgType::Positional ) {}

Argument::Argument(
    std::string name, std::string description, std::string short_opt,
    std::string long_opt )
    : _name( std::move( name ) ), _description( std::move( description ) ),
      _short( '-' + std::move( short_opt ) ),
      _long( long_opt.empty() ? std::string{} : "--" + std::move( long_opt ) ),
      _kind( ArgType::Flag ) {}

auto Argument::name() const -> std::string { return _name; }

auto Argument::description() const -> std::string { return _description; }

auto Argument::kind() const -> ArgType { return _kind; }

void Argument::match( std::string const & arg ) {
  switch ( _kind ) {
  case ArgType::Positional:
    _arg_matched = arg == _name;
    break;
  case ArgType::Flag:
    _arg_matched = ( _short == arg ) || ( _long.empty() ? false : _long == arg );
    break;
  };
}

auto Argument::matched() const -> bool { return _arg_matched; }

auto Argument::short_opt() const -> std::string { return _short; }

auto Argument::long_opt() const -> std::string { return _long; }

Parser::Parser( std::string banner, std::string version, std::string binary_name )
    : _binary_name( std::move( binary_name ) ), _banner( std::move( banner ) ),
      _version( std::move( version ) ) {}

void Parser::set_banner( std::string banner ) { _banner = std::move( banner ); }

void Parser::set_version( std::string version ) { _version = std::move( version ); }

void Parser::add_flag(
    std::string name, std::string description, std::string short_flag,
    std::string long_flag ) {
  Argument flag{
      std::move( name ), std::move( description ), std::move( short_flag ),
      std::move( long_flag ) };
  _args.insert( { flag.name(), std::move( flag ) } );
}

void Parser::add_positional_argument( std::string name, std::string description ) {
  Argument positional{ std::move( name ), std::move( description ) };
  _args.insert( { positional.name(), std::move( positional ) } );
}

void Parser::parse( int argc, char ** argv ) {
  std::vector<std::string> args(
      argv + 1, argv + argc ); // NOLINT(*-arithmetic) No std::span in C++17

  for ( auto const & arg : args ) {
    bool arg_matched = false;
    for ( auto & [ key, opt ] : _args ) {
      std::ignore = key;
      opt.match( arg );
      if ( opt.matched() ) {
        arg_matched = true;
        break;
      }
    }
    if ( !arg_matched ) {
      _unknown_args.emplace_back( arg );
    }
  }
}

auto Parser::unmatched_args() const -> std::vector<std::string> const & {
  return _unknown_args;
}

auto Parser::has_argument( std::string const & name ) -> bool {
  auto maybe_arg = _args.find( name );
  if ( maybe_arg == _args.end() ) {
    return false;
  }
  return maybe_arg->second.matched();
};

void Parser::print_help() {
  std::stringstream help{};

  std::vector<std::reference_wrapper<Argument>> positionals{};
  std::vector<std::reference_wrapper<Argument>> optionals{};

  int constexpr column_margin = 3;
  std::size_t colum_width = column_margin;

  if ( !_banner.empty() ) {
    help << _banner << '\n';
  }

  for ( auto & [ _, arg ] : _args ) {

    std::size_t arg_col_len =
        arg.long_opt().size() + arg.short_opt().size() + arg.name().size();
    if ( arg_col_len > colum_width ) {
      colum_width = arg_col_len + column_margin;
    }

    if ( arg.kind() == Argument::ArgType::Positional ) {
      positionals.emplace_back( std::ref( arg ) );
      continue;
    }
    optionals.emplace_back( std::ref( arg ) );
  }

  if ( !_binary_name.empty() ) {
    help << "Usage:"
         << "\n\t" << _binary_name;
    if ( !positionals.empty() ) {
      help << " <Arguments>";
    }
    if ( !optionals.empty() ) {
      help << " <Options>";
    }
    help << '\n';
  }

  if ( !positionals.empty() ) {
    help << "Arguments:" << '\n';
    for ( auto const & arg : positionals ) {
      auto const & pos = arg.get();
      std::stringstream line{};
      line << std::left << '\t' << std::setw( static_cast<int>( colum_width ) )
           << pos.name() << pos.description();
      help << line.str() << '\n';
    }
  }

  if ( !optionals.empty() ) {
    help << "Options:" << '\n';
    for ( auto const & arg : optionals ) {
      auto const & opt = arg.get();
      std::stringstream line{};
      line << std::left << '\t';
      std::string opt_string{};
      if ( !opt.short_opt().empty() ) {
        opt_string += opt.short_opt() + ' ';
      }
      if ( !opt.long_opt().empty() ) {
        opt_string += opt.long_opt();
      }
      line << std::setw( static_cast<int>( colum_width ) ) << opt_string;
      line << opt.description();
      help << line.str() << '\n';
    }
  }

  std::cout << help.str();
}

void Parser::print_version() { std::cout << _version << '\n'; }
} // namespace cuuid::util
