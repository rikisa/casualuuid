execute_process(
  COMMAND ${GIT_EXECUTABLE} describe --always --tags --long
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_STRIP_TRAILING_WHITESPACE
  OUTPUT_VARIABLE GIT_DESC
  RESULT_VARIABLE _result)

if(_result)
  set(GIT_DESC "dev")
  message(
    WARNING "Failed to invoke git exectuable to obtain revision information!")
endif()

string(REGEX MATCH "([0-9.]+)-([0-9]+)-(.*)" DESC_MATCH_FULL "${GIT_DESC}")

if(DESC_MATCH_FULL)
  set(VERSION "${CMAKE_MATCH_1}")
  set(REVISION "${CMAKE_MATCH_2}")
  set(HASHSUM "${CMAKE_MATCH_3}")
else()
  set(VERSION "dev")
  set(REVISION "")
  set(HASHSUM "${GIT_DESC}")
endif()

if(EXISTS ${GIT_TAG_FILE})
  file(READ ${GIT_TAG_FILE} _PREF_REV)
  # if revision changed, touch input -> reconfigures -> outdate versioninfo
  if(NOT "${_PREF_REV}" STREQUAL "${GIT_DESC}")
    message(TRACE "Git description changed ${_PREF_REV} -> ${GIT_DESC}")
    file(TOUCH ${VERINFO_INPUT})
    file(WRITE ${GIT_TAG_FILE} "${GIT_DESC}")
  else()
    message(TRACE "No changes in source control")
  endif()
else()
  message(TRACE "Creating git tagfile: ${GIT_TAG_FILE} <- ${GIT_DESC}")
  file(WRITE ${GIT_TAG_FILE} "${GIT_DESC}")
endif()

configure_file(${VERINFO_INPUT} ${VERINFO_OUTPUT} @ONLY)

set(VERSIONINFO_UPDATE_SCRIPT ${CMAKE_CURRENT_LIST_FILE})
