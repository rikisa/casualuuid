#include "casualuuid/argparser.hpp"
#include "casualuuid/cuuid.hpp"
#include "casualuuid/versioninfo.hpp"

#include <iostream>
#include <system_error>

auto main( int argc, char ** argv ) -> int {

  cuuid::util::Parser parser{
      "casualuuid - CLI tool to create UUIDs - " +
          std::string( VerisionInfo::long_version ),
      VerisionInfo::version.cbegin(), "cuuid-cli" };

  parser.add_flag( "UUID4", "Generate and print UUID4", "4" );
  parser.add_flag( "ver", "Print version and quit", "v", "version" );
  parser.add_flag( "help", "Print help and quit", "h" );

  parser.parse( argc, argv );

  // fail on unknown option
  if ( !parser.unmatched_args().empty() ) {
    std::cout << "Unknown option(s): ";
    for ( auto const & uopt : parser.unmatched_args() ) {
      std::cout << uopt << ' ';
    }
    std::cout << '\n';
    parser.print_help();
    return static_cast<int>( std::errc::invalid_argument );
  }

  if ( parser.has_argument( "ver" ) ) {
    parser.print_version();
    return 0;
  }

  if ( parser.has_argument( "help" ) ) {
    parser.print_help();
    return 0;
  }

  if ( parser.has_argument( "UUID4" ) ) {
    std::cout << cuuid::Uuid::create_uuid4() << '\n';
    return 0;
  }

  // fail on no option
  parser.print_help();
  return static_cast<int>( std::errc::invalid_argument );
}
