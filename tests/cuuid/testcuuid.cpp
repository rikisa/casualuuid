#include "casualuuid/argparser.hpp"
#include "casualuuid/cuuid.hpp"

#include <iostream>
#include <regex>
#include <system_error>

auto test_valid_v4() -> int {
  auto uuid4 = cuuid::Uuid::create_uuid4();

  std::stringstream sstream;
  sstream << uuid4;
  auto as_string = sstream.str();

  std::regex pattern{
      "[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[89ab][a-z0-9]{3}-[a-z0-9]{12}" };
  std::cmatch match;
  if ( std::regex_match( as_string.c_str(), match, pattern ) ) {
    return 0;
  } else {
    std::cout << "UUID4 could be not validated: " << as_string << '\n';
    // return -1;
  }

  auto version_byte =
      ( uuid4.byte_data().at( 6 ) & std::byte{ 0xf0 } ) == std::byte{ 0x40 };
  auto reserved_byte_low = uuid4.byte_data().at( 8 ) >= std::byte{ 0x8 };
  auto reserved_byte_up = uuid4.byte_data().at( 8 ) <= std::byte{ 0xb };

  if ( version_byte & reserved_byte_low & reserved_byte_up ) {
    return 0;
  } else {
    std::cout << "Special bytes are off:\n"
              << std::hex
              << "Version (0x4X): " << static_cast<int>( uuid4.byte_data().at( 6 ) )
              << '\n'
              << "Reserved (0x8X - 0xbX): "
              << static_cast<int>( uuid4.byte_data().at( 8 ) ) << '\n';
    return -1;
  }
}

auto main( int argc, char ** argv ) -> int {

  cuuid::util::Parser parser{ "cuuid tests", "dev", "test-cuuid" };

  parser.add_positional_argument( "valid-v4", "Test if the lib generates valid UUID4" );

  parser.parse( argc, argv );

  // fail on unknown option
  if ( !parser.unmatched_args().empty() ) {
    std::cout << "Unknown option(s): ";
    for ( auto const & uopt : parser.unmatched_args() ) {
      std::cout << uopt << ' ';
    }
    std::cout << '\n';
    parser.print_help();
    return static_cast<int>( std::errc::invalid_argument );
  }

  if ( parser.has_argument( "valid-v4" ) ) {
    return test_valid_v4();
  }

  // fail on no option
  parser.print_help();
  return static_cast<int>( std::errc::invalid_argument );
};
